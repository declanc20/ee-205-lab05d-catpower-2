///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file catpower.cpp
/// @version 1.0
///
/// @author Declan Campbell <declanc@hawaii.edu>
/// @date 13_Feb_2022
///////////////////////////////////////////////////////////////////////////////


#include"cat.h"

double fromJouleToCatPower( double joule){
return 0.0;
}

double fromCatPowerToJoule( double catPower ) {
   return 0.0;  // Cats do no work
}

