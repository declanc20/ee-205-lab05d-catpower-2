///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file foe.h
/// @version 1.0
///
/// @author Declan Campbell <declanc@hawaii.edu>
/// @date 13_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once
const double FOE_IN_A_JOULE = 1e-24;
const char FOE = 'f';


double fromFoeToJoule( double foe);

double fromJouleToFoe( double joule);
