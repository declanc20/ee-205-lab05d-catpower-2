///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file Megaton.h
/// @version 1.0
///
/// @author Declan Campbell <declanc@hawaii.edu>
/// @date 13_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

const double MEGATON_IN_A_JOULE = 2.39005736e-16;
const char MEGATON              = 'm';

extern double fromMegatonToJoule( double megaton );

extern double fromJouleToMegaton( double joule );

