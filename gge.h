///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.h
/// @version 1.0
///
/// @author Declan Campbell <declanc@hawaii.edu>
/// @date 13_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#pragma once
const double GASOLINE_GAS_IN_A_JOULE = 8.24402308e-9;
const char   GASOLINE_GALLON         = 'g';


double fromGasGallonToJoule( double gasGallon);

double fromJouleToGasGallon(double joule);
